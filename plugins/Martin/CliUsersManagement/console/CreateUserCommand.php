<?php

namespace Martin\CliUsersManagement\Console;

use Lang;
use BackendAuth;
use Backend\Models\User;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CreateUserCommand extends Command {

    protected $name = 'users:new';

    protected $description = 'Create default backend users';

    public function handle() {

        $user = BackendAuth::register([
            'first_name' => '',
            'last_name' => '',
            'login' => 'root',
            'email' => 'some@website.tld',
            'password' => 'changeme',
            'password_confirmation' => 'changeme'
        ]);

    }

}

?>
