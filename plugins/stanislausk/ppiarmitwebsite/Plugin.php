<?php namespace Stanislausk\PpiaRmitWebsite;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            '\Stanislausk\PpiaRmitWebsite\Components\MemberBenefitComponent' => 'MemberBenefitComponent',
            '\Stanislausk\PpiaRmitWebsite\Components\CommitteeMemberComponent' => 'CommitteeMemberComponent',
            '\Stanislausk\PpiaRmitWebsite\Components\EasyButton' => 'EasyButton',
            '\Stanislausk\PpiaRmitWebsite\Components\MemberRegistration' => 'MemberRegistration',
            '\Stanislausk\PpiaRmitWebsite\Components\InternalEventComponent' => 'InternalEventComponent',
            '\Stanislausk\PpiaRmitWebsite\Components\SocietalProjectPurchaseTicket' => 'SocietalProjectPurchaseTicket',
            '\Stanislausk\PpiaRmitWebsite\Components\PublicEventComponent' => 'PublicEventComponent',
        ];
    }

    public function registerSettings()
    {
        return [
          'config' => [
              'label'       => 'PPIA RMIT',
              'icon'        => 'icon-cog',
              'description' => 'PPIA RMIT Plugin Config',
              'class'       => 'Stanislausk\PpiaRmitWebsite\Models\Settings',
              'permissions' => ['stanislausk.ppiarmitwebsite.access_settings'],
              'order'       => 600
          ]
      ];
    }

    public function boot()
    {
        set_include_path(get_include_path() . PATH_SEPARATOR . __DIR__.'/vendor');
    }
}
