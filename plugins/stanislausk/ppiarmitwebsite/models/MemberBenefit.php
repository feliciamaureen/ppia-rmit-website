<?php namespace Stanislausk\PpiaRmitWebsite\Models;

use Model;

/**
 * Model
 */
class MemberBenefit extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /* Relations */
    public $attachOne = [
      'brandLogo' => 'System\Models\File'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'stanislausk_ppiarmitwebsite_member_benefit';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
