<?php namespace Stanislausk\PpiaRmitWebsite\Models;

use Model;

/**
 * Model
 */
class Committee extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /* Relations */
    public $belongsTo = [
        'member_role' => [
            'Stanislausk\PpiaRmitWebsite\models\CommitteeRole',
            'table' => 'stanislausk_ppiarmitwebsite_committee_role',
            'order' => 'role_name'
        ]
    ];

    public $attachOne = [
      'profilePic' => 'System\Models\File'
    ];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'stanislausk_ppiarmitwebsite_committee';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public function memberRole()
    {
        return $this->belongsTo('member_role');
    }
}
