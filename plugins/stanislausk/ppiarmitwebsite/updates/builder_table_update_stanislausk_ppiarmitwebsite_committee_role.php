<?php namespace Stanislausk\PpiaRmitWebsite\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateStanislauskPpiarmitwebsiteCommitteeRole extends Migration
{
    public function up()
    {
        Schema::table('stanislausk_ppiarmitwebsite_committee_role', function($table)
        {
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('stanislausk_ppiarmitwebsite_committee_role', function($table)
        {
            $table->dropColumn('deleted_at');
        });
    }
}
