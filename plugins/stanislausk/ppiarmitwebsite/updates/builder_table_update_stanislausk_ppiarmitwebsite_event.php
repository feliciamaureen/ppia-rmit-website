<?php namespace Stanislausk\PpiaRmitWebsite\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateStanislauskPpiarmitwebsiteEvent extends Migration
{
    public function up()
    {
        Schema::table('stanislausk_ppiarmitwebsite_event', function($table)
        {
            $table->string('text_color', 7)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('stanislausk_ppiarmitwebsite_event', function($table)
        {
            $table->dropColumn('text_color');
        });
    }
}
