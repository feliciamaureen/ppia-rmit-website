<?php namespace Stanislausk\PpiaRmitWebsite\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateStanislauskPpiarmitwebsiteCommitteeRole3 extends Migration
{
    public function up()
    {
        Schema::table('stanislausk_ppiarmitwebsite_committee_role', function($table)
        {
            $table->integer('display_order')->nullable()->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('stanislausk_ppiarmitwebsite_committee_role', function($table)
        {
            $table->dropColumn('display_order');
        });
    }
}
