<?php namespace Stanislausk\PpiaRmitWebsite\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateStanislauskPpiarmitwebsiteMemberBenefit extends Migration
{
    public function up()
    {
        Schema::create('stanislausk_ppiarmitwebsite_member_benefit', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('brand_name', 255);
            $table->string('benefit_description', 500);
            $table->string('brand_address', 300)->nullable();
            $table->string('brand_phone', 30)->nullable();
            $table->string('brand_website', 255)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('stanislausk_ppiarmitwebsite_member_benefit');
    }
}
