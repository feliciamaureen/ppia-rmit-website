<?php namespace Stanislausk\PpiaRmitWebsite\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateStanislauskPpiarmitwebsiteCommittee4 extends Migration
{
    public function up()
    {
        Schema::table('stanislausk_ppiarmitwebsite_committee', function($table)
        {
            $table->renameColumn('member_role', 'member_role_id');
        });
    }
    
    public function down()
    {
        Schema::table('stanislausk_ppiarmitwebsite_committee', function($table)
        {
            $table->renameColumn('member_role_id', 'member_role');
        });
    }
}
