<?php namespace Stanislausk\PpiaRmitWebsite\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateStanislauskPpiarmitwebsiteCommittee extends Migration
{
    public function up()
    {
        Schema::table('stanislausk_ppiarmitwebsite_committee', function($table)
        {
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('stanislausk_ppiarmitwebsite_committee', function($table)
        {
            $table->dropColumn('deleted_at');
        });
    }
}
