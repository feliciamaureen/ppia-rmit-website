<?php namespace Stanislausk\Ppiarmitwebsite\Components;

use Cms\Classes\ComponentBase;
use Stanislausk\Ppiarmitwebsite\models\Committee;

class CommitteeMemberComponent extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'PPIA RMIT Committee',
            'description' => 'Display the committee of PPIA RMIT'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getCommitteeMembers()
    {
        $byMember = [];
        $byGroupName = [];
        $byGroupId = [];
        $query = Committee::join(
            'stanislausk_ppiarmitwebsite_committee_role',
            'member_role_id',
            '=',
            'stanislausk_ppiarmitwebsite_committee_role.id'
        )
        ->select([
          'stanislausk_ppiarmitwebsite_committee.*',
          'stanislausk_ppiarmitwebsite_committee_role.display_order',
          ])
        ->orderBy('display_order');

        foreach ($query->get() as $committee) {
            $memberName = $committee->name;
            $memberGroup = $committee->member_role->role_name;
            $memberGroup_id = $committee->member_role->id;

            if (!isset($byGroup[$memberGroup])) {
                $byGroup[$memberGroup] = [
                  'name' => $memberGroup,
                  'members' => []
                ];
            }

            if (!isset($byGroupId[$memberGroup_id])) {
                $byGroupId[$memberGroup_id] = [
                  'name' => $memberGroup,
                  'members' => []
                ];
            }

            $member = [
              'name' => $memberName,
              'group' => $memberGroup,
              'profilePic' => null,
              'id' => $committee->id
            ];

            if (isset($committee->profilePic)) {
                $member['profilePic'] = $committee->profilePic->getPath();
                $member['profilePic_type_full'] = $committee->profilePic->content_type;
                preg_match(
                    '/(\S+)\//',
                    $member['profilePic_type_full'],
                    $member['profilePic_type']
                );

                if (sizeof($member['profilePic_type']) == 2) {
                    $member['profilePic_type'] = $member['profilePic_type'][1];
                } else {
                    $member['profilePic_type'] = 'unknown';
                }
            }

            array_push($byMember, $member);
            array_push($byGroup[$memberGroup]['members'], $member);
            array_push($byGroupId[$memberGroup_id]['members'], $member);
        }

        $ret = [
          'byGroup' => $byGroupName,
          'byMember' => $byMember,
          'byGroupId' => $byGroupId
        ];

        return $ret;
    }
}
