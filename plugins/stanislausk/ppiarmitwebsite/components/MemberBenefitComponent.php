<?php namespace Stanislausk\Ppiarmitwebsite\Components;

use Cms\Classes\ComponentBase;
use Stanislausk\Ppiarmitwebsite\models\MemberBenefit;

class MemberBenefitComponent extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Member Benefit',
            'description' => 'Display member benefit'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getBenefit()
    {
        $ret = [];

        foreach (MemberBenefit::all() as $benefit) {
            $benefit_push = [
              'brand_name' => $benefit->brand_name,
              'brand_logo' => null,
              'benefit_description' => $benefit->benefit_description,
              'brand_address' => $benefit->brand_address,
              'brand_phone' => $benefit->brand_phone,
              'brand_website' => $benefit->brand_website
            ];

            if (isset($benefit->brandLogo)) {
                $benefit_push['brand_logo'] = $benefit->brandLogo->getPath();
            }

            array_push($ret, $benefit_push);
        }

        return $ret;
    }
}
