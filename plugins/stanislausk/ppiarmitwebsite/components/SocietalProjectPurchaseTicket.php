<?php namespace Stanislausk\Ppiarmitwebsite\Components;

use Flash;
use Validator;
use AjaxException;
use ValidationException;
use ApplicationException;
use Cms\Classes\ComponentBase;
use Stanislausk\Ppiarmitwebsite\Models\Settings;

require_once 'stanislausKrisna/captchaValidator.php';
require_once 'giggsey/libphonenumber-for-php/vendor/autoload.php';
require_once 'google/google-api-php-client-2.2.2/vendor/autoload.php';

use Google_Client;
use Google_Service_Drive;
use Google_Service_Sheets;
use Google_Service_Drive_DriveFile;
use Google_Service_Drive_ValueRange;
use Google_Service_Sheets_ValueRange;

class SocietalProjectPurchaseTicket extends ComponentBase
{
    private $client;

    private $driveService;
    private $sheetService;

    // Dear lord, please forgive me for what I will commit is a sin
    private $accessKey = '{
  "type": "service_account",
  "project_id": "ppia-rmit-societalproject",
  "private_key_id": "e6b0f75b3231c9d901c644ef9d056f154c9b5c32",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDDkEVW4ID6KIhN\nBxBPpNiuuyIR3GHmFJ1w1YpJj3C1cyg/2993ElU0++cKRr2tuNRgmsG5XCbs2o7a\nIsF3REbeYXV5Sj06cm5/wL3VBkIPnvy2NR1yZJj6N4gJwLUnRIu8s4f1h3Nv7wvC\nXJOMMRBmO7xcVfM9fO0ZD2yOcuylhkMd/5RLKdX8jhYmpu9UjBpMe+5/vvx76VNB\no9A6T1Y8dWGxQzPnURF9Q97vlB8+RURij6LNN8E/eeKGd3h57MbGjduXKd/w3hNa\nBqGDoVa/M0FpEuM2jBwhK8lastDGnclv5E46vKlkqJdbAhLfRw8JuhZLVuFkD3fn\njpczBMGfAgMBAAECggEACrlVl0Vpk5sEMza8JguD6lXOsRLYHeLRg2EVjtUaOO9r\n38/LbRJVsJ/qDbE1Y42RvKTTEyDcU/t9y+gOARLlNy3TxrPtSZ0CApvSJrIlWzy8\neBo18Pjwccz4kJHyg4jwIYfpLVI6xukuSH+etuUirhVM549UofGNmuTMO7+BMWf9\nX77qY7ux28Gf4AYvlUpPpYelMWw6cVDaQMxZZaCHW1HGNzE2Z/jrsleIhOzA4khd\n24PldFWzi8o+oUG5bLe95/aMQ/Z5/UJoxVcT7BTn6gtDOoXt66M1mXUsl3XqmCZQ\n2dPlJlNq9j1rLs/MWTFlsmLN3AP+9UEbKkAIrx8fUQKBgQDzyutvG9UyLNlFs52M\nn5untCgDx95s51DP8kw6Zg6iyGR5VDZ0HwNUZIq2/qnkGZXTVwUiOBAqa6nOdgcg\nqDKYlBc2KojaA6qKiOfgXrqswyjg0QXI+i6h8Vrvq0k0tNBq9rzBkAtzfDKs4894\nZSP5YdIZtrqScbSJ656N2k/S2QKBgQDNWx8poFu/5v5szddWEIK845KYBnX2QsUU\nOW4x+auGOxHWnUlTlFKMlPeUuaWIVGTkfrcRe/SNMKjbCp18N3rhYqgJpHOxgT1O\nOTti0bn44vttY3gp7IC4nH3x5veaaYaiQ2tojLvNzYkeb4UfnRo7IEUWAnL6/KpP\nqbviQa79NwKBgQCrKygORN51ciPC2o+dslqZbtZBgU1b7TVCK3716b4Q8N4KieQJ\nt9Sgpk72mOszqfsV/Z0vH2nKPvlkal4qwa10j1WUxFx9peFRjzouwJQUxJmo71k+\neNnQ/iK47+xgQyuqTj7NMJO5q9Pwq6p5f5m4eMQJJdDtDUSTKYmTU1qEUQKBgEO5\n36cs2dWKazKbGH+r5lJBg6Fb0B+tma232x3kR6t3Y8SyDvLfofH3V0fXij5SBwxM\ntjev99Fw0N+cup+y9HXMwbqcYAOUqhFYUruei4UH5wAGpCS/eRxpkCftko98agit\nz4ilkXKobKZ7BR6OA46ob+B4Yk9gcRLg2bQfEYU1AoGBAMuTGlLzowVhqqGIE2bm\nhZukRAyMB5mpkBicyqgwZ0GuyQVWiEsaCvTXgi52Zy0abrckFEWiHb5PNNEkm5jW\ndZzL1Y/R81J/biMSf+9KTWqRfdDoEVmNjNc7Twm+LX531/04YrkgPzN1XLauobky\nmxcBP7xxI77OaMsIbY2qupjM\n-----END PRIVATE KEY-----\n",
  "client_email": "societalproject-reservation@ppia-rmit-societalproject.iam.gserviceaccount.com",
  "client_id": "111389406155023764370",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/societalproject-reservation%40ppia-rmit-societalproject.iam.gserviceaccount.com"
}';
    // TODO: Read config from file. Separate production and development access key like any sane men would do

    public function componentDetails()
    {
        return [
            'name'        => 'Societal Project Purchase Ticket',
            'description' => 'Create form to purchase ticket for societal project'
        ];
    }

    public function defineProperties()
    {
        return [
            'spreadsheetName' => [
                'description'       => 'Spreadsheet file to store the data',
                'title'             => 'GDrive Link to Spreadsheet',
                'default'           => '',
                'type'              => 'string',
                'validationPattern' => '/spreadsheets/d/([a-zA-Z0-9-_]+)',
                'validationMessage' => 'Not a valid spreadsheet link',
                'required'          => 'This field is required'
            ]
        ];
    }

    private function getSettings()
    {
        $settings = Settings::instance();

        if (!$settings->gapi_key) {
            throw new ApplicationException('Google API key not set');
        }

        return $settings;
    }

    private function getGoogleClient()
    {
        if (!isset($this->client)) {
            // $settings = $this->getSettings();
            //
            // if (!$settings->gapi_key) {
            //     throw new ApplicationException('Google API key not set');
            // }

            $client = new Google_Client();
            $auth = json_decode($this->accessKey, true);
            $client->setAuthConfig($auth);

            if ($client->isAccessTokenExpired()) {
                if ($client->getRefreshToken()) {
                    $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
                }
            }

            $client->addScope(Google_Service_Sheets::SPREADSHEETS);
            $client->addScope(Google_Service_Drive::DRIVE);

            $this->client = $client;
        }

        return $this->client;
    }

    private function getDriveService()
    {
        if (!isset($this->driveService)) {
            $service = new Google_Service_Drive($this->getGoogleClient());

            $this->driveService = $service;
        }

        return $this->driveService;
    }

    private function getSheetService()
    {
        if (!isset($this->sheetService)) {
            $service = new Google_Service_Sheets($this->getGoogleClient());

            $this->sheetService = $service;
        }

        return $this->sheetService;
    }

    private function validatePhoneNumber($input)
    {
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
        $phoneNumber;

        try {
            $phoneNumber = $phoneUtil->parse($input, "AU");
        } catch (\libphonenumber\NumberParseException $e) {
            return false;
        }

        return $phoneUtil->isValidNumber($phoneNumber);
    }

    private function formatPhoneNumber($input)
    {
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
        $phoneNumber;

        try {
            $phoneNumber = $phoneUtil->parse($input, "AU");
        } catch (\libphonenumber\NumberParseException $e) {
            return false;
        }

        return $phoneUtil->format($phoneNumber, \libphonenumber\PhoneNumberFormat::NATIONAL);
    }

    private function createFormValidator()
    {
        return Validator::make(
            post(),
            ['firstName' => [
              'required',
              'min:2'
            ],
            'lastName' => [
              'required',
              'min:2'
            ],
            'email' => [
              'required',
              'email'
            ],
            'ticketCount' => [
              'required',
              'min:1',
              'max:99'
            ]
          ]
      );
    }

    public function onSubmitForm()
    {
        /* Verify captcha */
        $captchaResult = validateCaptcha(post()['g-recaptcha-response']);
        if ($captchaResult['code'] != 0) {
            throw new AjaxException([
            'message' => $captchaResult['message']
          ]);
        }

        /* Validate data */
        $validator = $this->createFormValidator();

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        if (!$this->validatePhoneNumber(post('phoneNumber'))) {
            throw new ValidationException(['phoneNumber' => 'Not a valid australian phone number.']);
        }

        $data = post();
        $data['phoneNumber'] = $this->formatPhoneNumber($data['phoneNumber']);

        /* Store data in sheets */
        date_default_timezone_set('Australia/Melbourne');
        $values = [[
          date('m/d/Y H:i:s', time()),// Date created
          $data["firstName"] . ' ' . $data["lastName"], // First name & Last name
          $data["phoneNumber"], //Phone number
          $data["email"], // Email address
          $data["ticketCount"]
        ]];

        /* Prepare API message body */
        $body   = new Google_Service_Sheets_ValueRange(['values' => $values]);
        $options = array('valueInputOption' => 'RAW');

        /* Get sheet ID from URL */
        $matches;
        preg_match(
            '/\/spreadsheets\/d\/([a-zA-Z0-9-_]+)/',
            $this->property('spreadsheetName'),
            $matches
        );
        $spreadhseetId = $matches[1];

        /* Save data in sheets */
        $result = $this->getSheetService()->spreadsheets_values->append($spreadhseetId, 'A1:C1', $body, $options);
        Flash::success('Thank you for purchasing. Our staff will contact you soon.');
    }
}
