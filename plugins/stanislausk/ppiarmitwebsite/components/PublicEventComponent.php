<?php namespace Stanislausk\Ppiarmitwebsite\Components;

use DateTimeZone;
use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use Stanislausk\Ppiarmitwebsite\models\Events;

class PublicEventComponent extends ComponentBase
{
    private $tzList = null;

    public function componentDetails()
    {
        return [
            'name'        => 'Public Event',
            'description' => 'PPIA RMIT Public event display'
        ];
    }

    public function defineProperties()
    {
        return [
          'timezone' => [
            'title' => 'Timezone',
            'type' => 'dropdown'
          ],
          'dateTimeFormat' => [
            'title' => 'Datetime Format',
            'type' => 'string',
            'default' => 'F j, Y \a\t H:i A'
          ]
        ];
    }

    public function getTimezoneOptions()
    {
        if ($this->tzList === null) {
            $this->tzList = DateTimeZone::listIdentifiers(DateTimeZone::ALL);
        }

        return $this->tzList;
    }

    private function get_timezone_abbreviation($timezone_id)
    {
        if ($timezone_id) {
            $abb_list = timezone_abbreviations_list();

            $abb_array = array();
            foreach ($abb_list as $abb_key => $abb_val) {
                foreach ($abb_val as $key => $value) {
                    $value['abb'] = $abb_key;
                    array_push($abb_array, $value);
                }
            }

            foreach ($abb_array as $key => $value) {
                if ($value['timezone_id'] == $timezone_id) {
                    return strtoupper($value['abb']);
                }
            }
        }
        return false;
    }

    public function getPublicEvents()
    {
        $selectedTimezone = $this->getTimezoneOptions()[$this->property('timezone')];
        $tzAbbreviation = $this->get_timezone_abbreviation($selectedTimezone);
        $timeAtTimezone = Carbon::now($selectedTimezone);

        // Force the current time as UTC (because server time is using UTC)
        // Maybe change the config? Could work, eh? ¯\_(ツ)_/¯
        $timeAtTimezone_forceUTC = Carbon::create(
            $timeAtTimezone->year,
            $timeAtTimezone->month,
            $timeAtTimezone->day,
            $timeAtTimezone->hour,
            $timeAtTimezone->minute,
            $timeAtTimezone->second,
            'UTC'
        );

        $ret = [
          'other' => [],
          'headline' => []
        ];

        // Construct query
        $events = Events::where('hidden_at', '>', $timeAtTimezone_forceUTC)
        ->orderBy('event_date')->get();

        // Process result
        foreach ($events as $event) {
            $event_push = [
              'event_name' => $event->event_name,
              'event_cover' => null,
              'event_date' => date(
                  $this->property('dateTimeFormat'),
                  strtotime($event->event_date)
              ),
              'timezone' => $tzAbbreviation,
              'isHeadline' => $event->headline == 1,
              'short_description' => $event->short_description,
              'text_color' => $event->text_color,

              'learn_more_label' => null,
              'learn_more_link' => null,
              'learn_more_color' => $event->read_more_color
            ];

            if (isset($event->event_cover)) {
                $event_push['event_cover'] = $event->event_cover->getPath();
            }

            if (isset($event->read_more_link) && isset($event->read_more_label)) {
                $event_push['learn_more_label'] = $event->read_more_label;
                $event_push['learn_more_link'] = $event->read_more_link;
            }

            if ($event->headline == 1) {
                array_push($ret['headline'], $event_push);
            } else {
                array_push($ret['other'], $event_push);
            }
        }

        return $ret;
    }
}
