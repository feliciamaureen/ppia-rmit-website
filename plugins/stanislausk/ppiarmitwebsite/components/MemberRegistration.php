<?php namespace Stanislausk\Ppiarmitwebsite\Components;

/*
Update list of studies from https://www.international.rmit.edu.au/info/programfees.asp
*/

use Mail;
use Flash;
use Validator;
use AjaxException;
use ValidationException;
use ApplicationException;
use Cms\Classes\ComponentBase;
use Stanislausk\Ppiarmitwebsite\Models\Settings;

require_once 'stanislausKrisna/rmitProgramList.php';
require_once 'stanislausKrisna/captchaValidator.php';
require_once 'giggsey/libphonenumber-for-php/vendor/autoload.php';
require_once 'google/google-api-php-client-2.2.2/vendor/autoload.php';

use Google_Client;
use Google_Service_Drive;
use Google_Service_Sheets;
use Google_Service_Drive_DriveFile;
use Google_Service_Drive_ValueRange;
use Google_Service_Sheets_ValueRange;

class MemberRegistration extends ComponentBase
{
    private $client;

    private $driveService;
    private $sheetService;
    private $skipDataSaving = false;

    public function componentDetails()
    {
        return [
            'name'        => 'Registration form',
            'description' => 'New PPIA RMIT member registration form'
        ];
    }

    public function defineProperties()
    {
        return [
            'spreadsheetName' => [
                'description'       => 'Name of file to store new members list',
                'title'             => 'File name',
                'default'           => '',
                'type'              => 'string',
                'validationPattern' => '[a-zA-Z0-9-_]+',
                'validationMessage' => 'Name can only contain alphabet, numbers, underscore, and strip (-)',
                'required'          => 'This field is required'
            ]
        ];
    }

    private function getSettings()
    {
        $settings = Settings::instance();

        if (!$settings->gapi_key) {
            throw new ApplicationException('Google API key not set');
        }

        return $settings;
    }

    private function getGoogleClient()
    {
        if (!isset($this->client)) {
            $settings = $this->getSettings();

            if (!$settings->gapi_key) {
                throw new ApplicationException('Google API key not set');
            }

            $client = new Google_Client();
            $auth = json_decode($settings->gapi_key->getContents(), true);
            $client->setAuthConfig($auth);

            if ($client->isAccessTokenExpired()) {
                if ($client->getRefreshToken()) {
                    $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
                }
            }

            $client->addScope(Google_Service_Sheets::SPREADSHEETS);
            $client->addScope(Google_Service_Drive::DRIVE);

            $this->client = $client;
        }

        return $this->client;
    }

    private function getDriveService()
    {
        if (!isset($this->driveService)) {
            $service = new Google_Service_Drive($this->getGoogleClient());

            $this->driveService = $service;
        }

        return $this->driveService;
    }

    private function getSheetService()
    {
        if (!isset($this->sheetService)) {
            $service = new Google_Service_Sheets($this->getGoogleClient());

            $this->sheetService = $service;
        }

        return $this->sheetService;
    }

    /**
     * [searchSheetInDrive description]
     * @return String Return null if file not found
     */
    private function searchSheetInDrive()
    {
        $pageToken = null;

        do {
            $response = $this->getDriveService()->files->listFiles(array(
              'q' => "name='" . $this->property('spreadsheetName') . "' and mimeType='application/vnd.google-apps.spreadsheet'",
              'spaces' => 'drive',
              'pageToken' => $pageToken,
              'fields' => 'nextPageToken, files(id, name)',
            ));

            foreach ($response->files as $file) {
                return $file->id;
            }

            $pageToken = $response->pageToken;
        } while ($pageToken != null);

        return null;
    }

    private function getStorage()
    {
        $settings = Settings::instance();
        if ($settings->spreadsheet_url != '') {
            return $settings->spreadsheet_url;
        }

        $storageId = $this->searchSheetInDrive();

        if ($storageId == null) {
            throw new ApplicationException('Cannot find storage file. Update the settings and try again.');
        }

        return $storageId;
    }

    private function validatePhoneNumber($input)
    {
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
        $phoneNumber;

        try {
            $phoneNumber = $phoneUtil->parse($input, "AU");
        } catch (\libphonenumber\NumberParseException $e) {
            return false;
        }

        return $phoneUtil->isValidNumber($phoneNumber);
    }

    private function formatPhoneNumber($input)
    {
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
        $phoneNumber;

        try {
            $phoneNumber = $phoneUtil->parse($input, "AU");
        } catch (\libphonenumber\NumberParseException $e) {
            return false;
        }

        return $phoneUtil->format($phoneNumber, \libphonenumber\PhoneNumberFormat::NATIONAL);
    }

    private function createFormValidator()
    {
        return Validator::make(
            post(),
            ['firstName' => [
              'required',
              'min:2'
            ],
            'lastName' => [
              'required',
              'min:2'
            ],
            'dateOfBirth' => [
              'required',
              'date',
              'before:'.(date("Y") - 16).'-01-01'
            ],
            'email' => [
              'required',
              'email'
            ],
            'phoneNumber' => [
              'required',
              'min:7'
            ],
            'studentNumber' => [
              'required',
              'min:7'
            ],
            'areaOfStudy' => [
              'required',
              'min:2'
            ],
            'completionDate' => [
              'required',
              'date'
            ]
          ]
      );
    }

    private function normalizeStudentId($id)
    {
    	$id = strtolower($id);

    	if($id[0] !== 's') {
    		$id = 's' . $id;
    	}

    	return $id;
    }

    public function onRender()
    {
      $this->page['programs'] = getRmitProgramList();
    }

    public function onSubmitForm()
    {
        /* Verify captcha */
        $captchaResult = validateCaptcha(post()['g-recaptcha-response']);
        if ($captchaResult['code'] != 0) {
            throw new AjaxException([
              'message' => $captchaResult['message']
            ]);
        }

        $settings = $this->getSettings();

        /* Process data */
        $validator = $this->createFormValidator();

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        if (!$this->validatePhoneNumber(post('phoneNumber'))) {
            throw new ValidationException(['phoneNumber' => 'Not a valid australian phone number.']);
        }

        $data = post();
        $data['phoneNumber'] = $this->formatPhoneNumber($data['phoneNumber']);
        $newMemberName = $data["firstName"] . ' ' . $data["lastName"];
        $memberEmail = $data["email"];
        $data["studentNumber"] = $this->normalizeStudentId($data["studentNumber"]);

        /* Store data in sheets */
        date_default_timezone_set('Australia/Melbourne');
        $values = [[
          date('m/d/Y H:i:s', time()),// Date created
          $newMemberName, // First name & Last name
          $data["phoneNumber"], //Phone number
          $memberEmail, // Email address
          $data["studentNumber"], // Student number
          $data["dateOfBirth"], // Date of birth
          $data["areaOfStudy"], // Level and area of study
          $data["completionDate"], // Completion date,
          "" // Card number
          ]];
        $body   = new Google_Service_Sheets_ValueRange(['values' => $values]);
        $options = array('valueInputOption' => 'RAW');

        $spreadhseetId = $this->getStorage();
        if (!$this->skipDataSaving) {
            $result = $this->getSheetService()->spreadsheets_values->append($spreadhseetId, 'A1:C1', $body, $options);
        }

        // Send welcome email
        $welcomeEmail_vars = ['name' => $newMemberName];
        $sendEmailBody = function ($message) use ($memberEmail, $newMemberName) {
            $message->to($memberEmail, $newMemberName);
            $message->subject('Welcome to PPIA RMIT, ' . $newMemberName);
        };
        try {
            Mail::send('external::invite.welcome', $welcomeEmail_vars, $sendEmailBody);
        } catch (Exception $e) {
            // Do nothing lol
        }

        Flash::success('You have been registered. Our team will contact you for your card');

        return true;
    }
}
