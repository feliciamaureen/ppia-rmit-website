<?php namespace Stanislausk\Ppiarmitwebsite\Components;

use Cms\Classes\ComponentBase;

class EasyButton extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'EasyButton',
            'description' => 'Create button element with ease'
        ];
    }

    public function defineProperties()
    {
        return [
          'btnId' => [
            'title'             => 'ID',
            'description'       => 'Button ID',
            'default'           => '',
            'type'              => 'string',
            'validationPattern' => '^[A-Za-z][A-Za-z\d-_:.]*$',
            'validationMessage' => 'ID must start with alphabet and can only contain alphabet, numbers, hyphen (-), underscore (_), colon (:), and period (.)'
          ],
          'btnClass' => [
            'title'             => 'Class',
            'description'       => 'Button Class',
            'default'           => '',
            'type'              => 'string',
            'validationPattern' => '^[A-Za-z\d-_:. ]*$',
            'validationMessage' => 'Class can only contain alphabet, numbers, hyphen (-), underscore (_), colon (:), and period (.)'
          ],
          'btnType' => [
            'title'             => 'Type',
            'description'       => 'Button Type',
            'default'           => 'button',
            'type'              => 'string',
            'validationPattern' => '^[A-Za-z]*$',
            'validationMessage' => 'Class can only contain alphabet'
          ],
          'btnText' => [
            'title'             => 'Text',
            'description'       => 'Button Text',
            'default'           => 'Button',
            'type'              => 'string',
            'validationPattern' => '',
            'validationMessage' => ''
          ],
          'btnDisabled' => [
            'title'             => 'Disabled',
            'description'       => 'Disable the button',
            'default'           => false,
            'type'              => 'checkbox',
            'validationPattern' => '',
            'validationMessage' => ''
          ],
      ];
    }
}
