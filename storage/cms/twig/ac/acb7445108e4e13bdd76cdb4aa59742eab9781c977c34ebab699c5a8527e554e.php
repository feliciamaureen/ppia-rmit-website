<?php

/* /Users/stanislauskrisna/Documents/ppia_rmit_website/themes/demo/pages/404.htm */
class __TwigTemplate_dbaf81ddc05f4eff33c47304dfa75b3ff0b32b5a48e6fe84d068017dfa9593cc extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"jumbotron\">
    <div class=\"container\">
        <h1>Page not found</h1>
        <p>We're sorry, but the page you requested cannot be found.</p>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/Users/stanislauskrisna/Documents/ppia_rmit_website/themes/demo/pages/404.htm";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"jumbotron\">
    <div class=\"container\">
        <h1>Page not found</h1>
        <p>We're sorry, but the page you requested cannot be found.</p>
    </div>
</div>", "/Users/stanislauskrisna/Documents/ppia_rmit_website/themes/demo/pages/404.htm", "");
    }
}
